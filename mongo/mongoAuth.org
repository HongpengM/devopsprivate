* definition 
** Start MongoDB without access control.
   Start a standalone mongod instance without access control.

   For example, open a terminal and issue the following:
   #+BEGIN_SRC sh
   mongod --port 27017 --dbpath /var/lib/mongodb
   #+END_SRC

** Connect to the instance

   #+BEGIN_SRC sh
   mongo --port 27017
   #+END_SRC

** Create the user administrator

   #+BEGIN_SRC js
     use admin
     db.createUser(
       {
         user: "myUserAdmin",
         pwd: "abc123",
         roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
       }
     )        
   #+END_SRC

** Re-start the MongoDB instance with access control.

   Shut down the mongod instance. For example, from the mongo shell, issue the following command:
   #+BEGIN_SRC 
   db.adminCommand( { shutdown: 1 } )   
   #+END_SRC

   Exit the mongo shell.
   
   From the terminal, re-start the mongod instance with the --auth command line option or, if using a configuration file, the security.authorization setting.
   #+BEGIN_SRC 
   mongod --auth --port 27017 --dbpath /var/lib/mongodb
   #+END_SRC
   Clients that connect to this instance must now authenticate themselves as a MongoDB user. Clients can only perform actions as determined by their assigned roles.


** Connect and authenticate as the user administrator

*** Authentication during the connection
    #+BEGIN_SRC 
    mongo --port 27017 -u "myUserAdmin" --authenticationDatabase "admin" -p
    #+END_SRC


** Create additional users as needed for your deployment

   #+BEGIN_SRC 
    use test
    db.createUser(
      {
        user: "myTester",
        pwd: "xyz123",
        roles: [ { role: "readWrite", db: "test" },
                 { role: "read", db: "reporting" } ]
      }
    )
   #+END_SRC
