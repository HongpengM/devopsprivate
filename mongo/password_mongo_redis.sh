docker run \
       -p 27017:27017 \
       --name mongo_container \
       -e MONGO_INITDB_ROOT_USERNAME=admin \
       -e MONGO_INITDB_ROOT_PASSWORD=tokyomaster443 \
       -e AUTH=yes \
       -v $PWD/env/mongo_data/db:/data/db \
       -d mongo:4.0 mongod 

echo "MongoDB container started"


docker run \
       -p 6379:6379 \
       -v $PWD/env/redis_data:/data:rw \
       --name redis_container \
       -d redis:5.0 redis-server --requirepass "tokyomaster123"  --appendonly yes
echo "Redis container started"       
