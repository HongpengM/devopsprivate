#!/bin/expect
set port 27017
set adminUser true
set userName "UserExample"
set userPassword "AbcdQwer1234"
spawn echo $userName
spawn echo ${userPassword}
spawn mongo --port ${port}
expect {
    "> " {
	send -- "show dbs\r"
	send -- "use admin\r"
	interact
    }
}
expect {
    "> " {
	send -- 'db.createUser(\n'
	send -- ' { user: '
	send -- $userName
	interact
	send -- ',\n'
	send -- ' pwd: '
	send -- ${userPassword}
	send --',\n'
	send -- ' roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]\n'
	send -- ' }\n'
	send -- ')\r}'
	interact
	exp_continue
    }    
}
exit
