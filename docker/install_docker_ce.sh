#!/bin/bash
# remove old version
sudo apt-get remove docker docker-engine docker.io containerd runc -y
# install dependencies
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y

# Add docker official GPG's key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add docker apt repo
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Install docker CE

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

# Test installed docker
sudo docker run hello-world

# Add current user(ubuntu) to docker group
sudo usermod -aG docker ubuntu
