#!/bin/bash

pathPrefix=~/Shells
cd ~/Shells
echo `ls`
arrLen=0
for file in `cd $pathPrefix  && ls hongpengm/ | grep sh`
do
    array[$arrLen]="$file"
    ((arrLen++))
done;

echo "Select the server you would like to connect"
for i in $(seq 1 ${#array[@]});
do echo $i ${array[$i]};
done;

scriptSelectAllow=false
while ! $scriptSelectAllow
do
    echo -n "ENTER server/script: "
    read scriptIdx
    if  [[ ! $scriptIdx =~ ^[0-9]+$ ]]; then
	echo -n "Input not allowed"
    else
	scriptSelectAllow=true
    fi
done

echo $scriptIdx
scriptPath="./hongpengm/${array[$scriptIdx]}"

echo $scriptPath
if [[ ${array[$scriptIdx]} =~ ^"exp" ]]; then
    expect $scriptPath
else
    bash $scriptPath
fi;

