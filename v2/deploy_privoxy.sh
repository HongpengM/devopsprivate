#!/bin/bash
sudo apt-get install privoxy
echo "
listen-address 127.0.0.1:8234
forward-socks5 / localhost:1080 .
" >> /etc/privoxy/config


if [[ `readlink /proc/$$/exe` == '/bin/zsh' ]]; then
    shell="zsh"

elif [[ `readlink /proc/$$/exe` == '/bin/bash' ]]; then
    shell="bash"
fi;

echo "
function proxy_off(){
    unset http_proxy
    unset https_proxy
    unset no_proxy
    echo -e \"已关闭代理\"
}

function proxy_on() {
    export no_proxy=\"localhost,127.0.0.1,localaddress,.localdomain.com\"
    export http_proxy=\"http://127.0.0.1:8234\"
    export https_proxy=$http_proxy
    echo -e \"已开启代理\"
}

alias terminalProxy=\"/usr/sbin/privoxy /etc/privoxy/config\"
" >> ~/.${shell}rc

source ~/.${shell}rc
terminalProxy
