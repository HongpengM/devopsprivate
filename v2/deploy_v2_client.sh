#Check Root
[ $(id -u) != "0" ] && { echo "${CFAILURE}Error: You must be root to run this script${CEND}"; exit 1; }

#Check OS
if [ -f /etc/redhat-release ];then
    OS='CentOS'
elif [ ! -z "`cat /etc/issue | grep bian`" ];then
    OS='Debian'
elif [ ! -z "`cat /etc/issue | grep Ubuntu`" ];then
    OS='Ubuntu'
else
    echo "Not support OS, Please reinstall OS and retry!"
    exit 1
fi

uuid=$(/usr/bin/v2ray/v2ctl uuid)
echo $uuid >> v2ray.uuid.txt

# Get Public IP address
ipc=$(ip addr | egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | egrep -v "^192\.168|^172\.1[6-9]\.|^172\.2[0-9]\.|^172\.3[0-2]\.|^10\.|^127\.|^255\.|^0\." | head -n 1)
if [[ "$IP" = "" ]]; then
    ipc=$(wget -qO- -t1 -T2 ip.sb)
fi


function Install(){
    #Install Basic Packages
    if [[ ${OS} == 'CentOS' ]];then
	yum install curl wget unzip ntp ntpdate -y
    else
	apt-get update
	apt-get install curl unzip ntp wget ntpdate -y
    fi

    #Set DNS
    echo "nameserver 8.8.8.8" >> /etc/resolv.conf
    echo "nameserver 8.8.4.4" >> /etc/resolv.conf


    #Update NTP settings
    rm -rf /etc/localtime
    ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
    ntpdate us.pool.ntp.org

    #Disable SELinux
    if [ -s /etc/selinux/config ] && grep 'SELINUX=enforcing' /etc/selinux/config; then
	sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
	setenforce 0
    fi

    #Run Install
    cd /root

    bash <(curl -L -s https://install.direct/go.sh)

}
clear
echo "Installation Completed!"


service v2ray stop

rm $PWD/config.json
cat <<EOF > $PWD/config.json
{
  "dns" : {
    "servers" : [
      "localhost"
    ]
  },
  "inbounds" : [
    {
      "listen" : "127.0.0.1",
      "port" : 1080,
      "protocol" : "socks",
      "tag" : "socksinbound",
      "settings" : {
        "auth" : "noauth",
        "udp" : true,
        "ip" : "127.0.0.1"
      }
    },
    {
      "listen" : "127.0.0.1",
      "port" : 8000,
      "protocol" : "http",
      "tag" : "httpinbound",
      "settings" : {
        "timeout" : 0
      }
    }
  ],
  "outbounds" : [
    {
      "sendThrough" : "0.0.0.0",
      "mux" : {
        "enabled" : false,
        "concurrency" : 8
      },
      "protocol" : "vmess",
      "settings" : {
        "vnext" : [
          {
            "address" : "52.197.181.62",
            "users" : [
              {
                "id" : "305e09b3-bc1a-8c7a-c01c-c6c326a0bccf",
                "alterId" : 64,
                "security" : "auto",
                "level" : 0
              }
            ],
            "port" : 10846
          }
        ]
      },
      "tag" : "V2ray JP",
      "streamSettings" : {
        "sockopt" : {

        },
        "quicSettings" : {
          "key" : "",
          "security" : "none",
          "header" : {
            "type" : "none"
          }
        },
        "tlsSettings" : {
          "allowInsecure" : false,
          "alpn" : [
            "http\/1.1"
          ],
          "serverName" : "server.cc",
          "allowInsecureCiphers" : false
        },
        "wsSettings" : {
          "path" : "",
          "headers" : {

          }
        },
        "httpSettings" : {
          "path" : "",
          "host" : [
            ""
          ]
        },
        "tcpSettings" : {
          "header" : {
            "type" : "none"
          }
        },
        "kcpSettings" : {
          "header" : {
            "type" : "none"
          },
          "mtu" : 1350,
          "congestion" : false,
          "tti" : 20,
          "uplinkCapacity" : 5,
          "writeBufferSize" : 1,
          "readBufferSize" : 1,
          "downlinkCapacity" : 20
        },
        "security" : "none",
        "network" : "tcp"
      }
    },
    {
      "sendThrough" : "0.0.0.0",
      "mux" : {
        "enabled" : false,
        "concurrency" : 8
      },
      "protocol" : "vmess",
      "settings" : {
        "vnext" : [
          {
            "address" : "182.61.105.168",
            "users" : [
              {
                "id" : "3963834d-3caf-4f2f-55b8-573fcea2a498",
                "alterId" : 64,
                "security" : "auto",
                "level" : 0
              }
            ],
            "port" : 32000
          }
        ]
      },
      "tag" : "HKServer",
      "streamSettings" : {
        "wsSettings" : {
          "path" : "",
          "headers" : {

          }
        },
        "quicSettings" : {
          "key" : "",
          "header" : {
            "type" : "none"
          },
          "security" : "none"
        },
        "tlsSettings" : {
          "allowInsecure" : false,
          "alpn" : [
            "http\/1.1"
          ],
          "serverName" : "server.cc",
          "allowInsecureCiphers" : false
        },
        "httpSettings" : {
          "path" : ""
        },
        "kcpSettings" : {
          "header" : {
            "type" : "none"
          },
          "mtu" : 1350,
          "congestion" : false,
          "tti" : 20,
          "uplinkCapacity" : 5,
          "writeBufferSize" : 1,
          "readBufferSize" : 1,
          "downlinkCapacity" : 20
        },
        "tcpSettings" : {
          "header" : {
            "type" : "http",
            "request" : {
              "path" : [
                "\/"
              ],
              "method" : "GET",
              "headers" : {
                "Host" : [
                  "www.baidu.com",
                  "www.bing.com"
                ],
                "Accept-Encoding" : [
                  "gzip, deflate"
                ],
                "Connection" : [
                  "keep-alive"
                ],
                "Pragma" : "no-cache",
                "User-Agent" : [
                  "Mozilla\/5.0 (Windows NT 10.0; WOW64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/53.0.2785.143 Safari\/537.36",
                  "Mozilla\/5.0 (iPhone; CPU iPhone OS 10_0_2 like Mac OS X) AppleWebKit\/601.1 (KHTML, like Gecko) CriOS\/53.0.2785.109 Mobile\/14A456 Safari\/601.1.46"
                ]
              },
              "version" : "1.1"
            },
            "response" : {
              "status" : "200",
              "reason" : "OK",
              "headers" : {
                "Connection" : [
                  "keep-alive"
                ],
                "Transfer-Encoding" : [
                  "chunked"
                ],
                "Pragma" : "no-cache",
                "Content-Type" : [
                  "application\/octet-stream",
                  "video\/mpeg"
                ]
              },
              "version" : "1.1"
            }
          }
        },
        "security" : "none",
        "network" : "tcp"
      }
    }
  ],
  "routings" : [
    {
      "name" : "all_to_main",
      "domainStrategy" : "AsIs",
      "rules" : [
        {
          "type" : "field",
          "outboundTag" : "main",
          "port" : "0-65535"
        }
      ]
    },
    {
      "name" : "bypasscn_private_apple",
      "domainStrategy" : "IPIfNonMatch",
      "rules" : [
        {
          "type" : "field",
          "outboundTag" : "direct",
          "domain" : [
            "localhost",
            "domain:me.com",
            "domain:lookup-api.apple.com",
            "domain:icloud-content.com",
            "domain:icloud.com",
            "domain:cdn-apple.com",
            "domain:apple-cloudkit.com",
            "domain:apple.com",
            "domain:apple.co",
            "domain:aaplimg.com",
            "domain:guzzoni.apple.com",
            "geosite:cn"
          ]
        },
        {
          "type" : "field",
          "outboundTag" : "direct",
          "ip" : [
            "geoip:private",
            "geoip:cn"
          ]
        },
        {
          "type" : "field",
          "outboundTag" : "main",
          "port" : "0-65535"
        }
      ]
    },
    {
      "name" : "all_to_direct",
      "domainStrategy" : "AsIs",
      "rules" : [
        {
          "type" : "field",
          "outboundTag" : "direct",
          "port" : "0-65535"
        }
      ]
    },
    {
      "domainStrategy" : "IPIfNonMatch",
      "rules" : [
        {
          "type" : "field",
          "ip" : [
            "0.0.0.0\/8",
            "10.0.0.0\/8",
            "100.64.0.0\/10",
            "127.0.0.0\/8",
            "169.254.0.0\/16",
            "172.16.0.0\/12",
            "192.0.0.0\/24",
            "192.0.2.0\/24",
            "192.168.0.0\/16",
            "198.18.0.0\/15",
            "198.51.100.0\/24",
            "203.0.113.0\/24",
            "::1\/128",
            "fc00::\/7",
            "fe80::\/10"
          ],
          "outboundTag" : "direct"
        },
        {
          "type" : "field",
          "outboundTag" : "main",
          "port" : "0-65535"
        }
      ],
      "name" : "some rule set"
    }
  ],
  "log" : {
    "error" : "/etc/v2ray/logs/error.log",
    "loglevel" : "debug",
    "access" : "/etc/v2ray/logs/access.log"
  }
}
 
EOF


rm /etc/v2ray/config.back
mv /etc/v2ray/config.json /etc/v2ray/config.back
mv $PWD/config.json /etc/v2ray/config.json
rm $PWD/config.json

mkdir -p /etc/v2ray/logs
cat '' >> /etc/v2ray/logs/access.log
cat '' >> /etc/v2ray/logs/error.log


service v2ray start

echo "v2ray service started!"
echo "socks on localhost:1080"
echo "http on localhost:8000"
echo "Test run: /usr/bin/v2ray/v2ray -config=/etc/v2ray/config.json"
